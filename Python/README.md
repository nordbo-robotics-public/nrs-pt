Find the correct IP-address of the NRS-PT and set it in the example code before compiling. Open a terminal in the `NRS-PT/Linux/C++/` directory and run the following command to run the example.

```
python nrs_pt_communication.py
```