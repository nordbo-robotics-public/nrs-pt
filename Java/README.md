Find the correct IP-address of the NRS-PT and set it in the example code before compiling. Open a terminal in the `NRS-PT/Linux/C++/` directory and run the following commands to compile and run the example.

```
javac nrs_pt_communication.java
java nrs_pt_communication
```