/***********************************************************************
*
* NRS-PT Ethernet TCP/IP example.
*
* ----------------------------------------------------------------------
*
* Author: Thomas Christensen <thomas@nordbo-robotics.com>
* Date: 13-11-2019
*
* ----------------------------------------------------------------------
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
************************************************************************/

import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class nrs_pt_communication {

	private static final String NRS_PT_IP_ADDR 	= "192.168.1.100";
	private static final int 	NRS_PT_PORT			= 2002;

	private static final byte NRS_PT_CMD_TRANSMIT 		= 0x01;
	private static final byte NRS_PT_CMD_TRANSMIT_STOP 	= 0x00;
	private static final byte NRS_PT_CMD_TRANSMIT_START = 0x01;

	private static final byte NRS_PT_FAILURE = 0x00;
	private static final byte NRS_PT_SUCCESS = 0x01;

	private static Socket socket = null;
	private static OutputStream output = null;	
	private static InputStream input = null;

	public static void main(String[] args) throws IOException {
		System.out.println("Connecting to NRS-PT on address: " + NRS_PT_IP_ADDR);

		socket = new Socket(NRS_PT_IP_ADDR, NRS_PT_PORT);
		output = socket.getOutputStream();
		input = socket.getInputStream();

		System.out.println("Connected");
		
		byte[] responsePacket = new byte[253];
		byte[] requestPacket  = new byte[3];

		requestPacket[0] = 0x03;
		requestPacket[1] = NRS_PT_CMD_TRANSMIT;
		requestPacket[2] = NRS_PT_CMD_TRANSMIT_START;
		
		output.write(requestPacket);
		
		responsePacket = recvPacket();

		if(responsePacket[2] == NRS_PT_SUCCESS)
	    {
	        System.out.println("Transmission started:");
	    }
	    else
	    {
	        //The request was denied, insert error handling
	    	System.out.println("Request denied. Exiting");
	        System.exit(1);
	    }

		double x, y, z, w, rx, ry, rz;
		DecimalFormatSymbols dfs = new DecimalFormatSymbols();
		dfs.setDecimalSeparator('.');
		DecimalFormat df = new DecimalFormat("0.00000",dfs);

		for(int i = 0; i < 5; i++) {
	        responsePacket = recvPacket();
	        x  = ByteBuffer.wrap(Arrays.copyOfRange(responsePacket, 2, 10)).getDouble();
	        y  = ByteBuffer.wrap(Arrays.copyOfRange(responsePacket, 10, 18)).getDouble();
	        z  = ByteBuffer.wrap(Arrays.copyOfRange(responsePacket, 18, 26)).getDouble();
	        rz = ByteBuffer.wrap(Arrays.copyOfRange(responsePacket, 26, 34)).getDouble();
	        w  = ByteBuffer.wrap(Arrays.copyOfRange(responsePacket, 34, 42)).getDouble();
	        rx = ByteBuffer.wrap(Arrays.copyOfRange(responsePacket, 42, 50)).getDouble();
	        ry = ByteBuffer.wrap(Arrays.copyOfRange(responsePacket, 50, 58)).getDouble();
			System.out.println("[" 	+ df.format(x) + "," + df.format(y) + "," + df.format(z) + ","
									+ df.format(w) + "," + df.format(rx) + ","+ df.format(ry) + ","
									+ df.format(rz) + "]");
		}

		System.out.println("Requesting stop of transmission");
		requestPacket[0] = 0x03;
		requestPacket[1] = NRS_PT_CMD_TRANSMIT;
		requestPacket[2] = NRS_PT_CMD_TRANSMIT_STOP;
		
		output.write(requestPacket);

		//Wait until the response for the stop request is received.
		do {
			responsePacket = recvPacket();
		} while(responsePacket[1] != NRS_PT_CMD_TRANSMIT);
		
		output.close();
		input.close();
		socket.close();
		System.exit(0);
	}

	private static byte[] recvPacket() throws IOException {
		byte[] recvData = new byte[253];
		int received = input.read(recvData, 0, 2);
		
		while(received < recvData[0]) {
			received += input.read(recvData, received, recvData[0] - received);
		}

		return recvData;
	}
};
