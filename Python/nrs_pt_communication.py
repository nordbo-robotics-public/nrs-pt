import struct
import socket

NRS_PT_CMD_TRANSMIT = '01'
NRS_PT_CMD_TRANSMIT_STOP = '00'
NRS_PT_CMD_TRANSMIT_START = '01'

NRS_PT_FAILURE = 0x00
NRS_PT_SUCCESS = 0x01

NRS_PT_PORT = 2002
NRS_PT_IP_ADDRESS = '192.168.1.100'

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

def main():
    global s
    print('Connecting to NRS-PT on address: ' + NRS_PT_IP_ADDRESS)
    s.settimeout(2.0)
    s.connect((NRS_PT_IP_ADDRESS, NRS_PT_PORT))
    print('Connected')

    #Initialise transmission of poses
    print('Requesting start of transmission.')
    requestPacket = '03' + NRS_PT_CMD_TRANSMIT + NRS_PT_CMD_TRANSMIT_START
    requestPacket = bytearray.fromhex(requestPacket)
    s.send(requestPacket)

    #Receive and handle the response
    responsePacket = recvPacket()
    if responsePacket[2] == NRS_PT_SUCCESS:
        print('Transmission started:')
    else:
        #The request was denied, insert error handling
        print('Request denied. Exiting')
        sys.exit(1)

    x = 0.0
    y = 0.0
    z = 0.0
    w = 0.0
    rx = 0.0
    ry = 0.0
    rz = 0.0

    for x in range(0,5):
        responsePacket = recvPacket()
        x = struct.unpack('!d', responsePacket[2:10])[0]
        y = struct.unpack('!d', responsePacket[10:18])[0]
        z = struct.unpack('!d', responsePacket[18:26])[0]
        w = struct.unpack('!d', responsePacket[26:34])[0]
        rx = struct.unpack('!d', responsePacket[34:42])[0]
        ry = struct.unpack('!d', responsePacket[42:50])[0]
        rz = struct.unpack('!d', responsePacket[50:58])[0]
        pose =[x,y,z,w,rx,ry,rz]
         
        print('[{0}]'.format(', '.join(map(str, ['{0:0.5f}'.format(x) for x in pose]))))

    print('Requesting stop of transmission.')
    requestPacket = '03' + NRS_PT_CMD_TRANSMIT + NRS_PT_CMD_TRANSMIT_STOP
    requestPacket = bytearray.fromhex(requestPacket)
    s.send(requestPacket)

    while True:
        responsePacket = recvPacket()
        if responsePacket[1] == int(NRS_PT_CMD_TRANSMIT):
            break
    
    s.shutdown(socket.SHUT_RDWR)
    s.close()


def recvPacket():
    recvPacket = bytearray(s.recv(2))

    while len(recvPacket) < recvPacket[0]:
        recvPacket += bytearray(s.recv(recvPacket[0] - len(recvPacket)))

    return recvPacket


if __name__ == "__main__":
    main()