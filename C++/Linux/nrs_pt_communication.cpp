/***********************************************************************
*
* NRS-PT Ethernet TCP/IP example.
*
* ----------------------------------------------------------------------
*
* Author: Thomas Christensen <thomas@nordbo-robotics.com>
* Date: 13-11-2019
*
* ----------------------------------------------------------------------
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
************************************************************************/

//Std
#pragma comment(lib, "ws2_32.lib")
#include <iostream>
#include <iomanip>

//Linux Socket
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>


#define NRS_PT_IP_ADDR  "192.168.1.100"
#define NRS_PT_PORT     2002

#define NRS_PT_CMD_TRANSMIT         0x01
#define NRS_PT_CMD_TRANSMIT_STOP    0x00
#define NRS_PT_CMD_TRANSMIT_START	0x01

void recvPacket(char *msg);
double bytesToDouble(const char *bytes);

int socket_desc;

int main()
{

	socket_desc = socket(AF_INET, SOCK_STREAM, 0);
	struct sockaddr_in server;

	server.sin_addr.s_addr = inet_addr(NRS_PT_IP_ADDR);
	server.sin_family = AF_INET;
	server.sin_port = htons(NRS_PT_PORT);
 
 	std::cout << "Connecting to NRS-PT on address: " << NRS_PT_IP_ADDR << std::endl;
	
	if(connect(socket_desc , (struct sockaddr *)&server , sizeof(server)) < 0) {
		return 1;
	}

    std::cout << "Connected" << std::endl;

    char responsePacket[253] = {0};
    char requestPacket[3]    = {0};

    //Initialise transmission of poses
    std::cout << "Requesting start of transmission" << std::endl;
    requestPacket[0] = 0x03;
    requestPacket[1] = NRS_PT_CMD_TRANSMIT;
    requestPacket[2] = NRS_PT_CMD_TRANSMIT_START;

    send(socket_desc, requestPacket, requestPacket[0], 0);

    recvPacket(responsePacket);

    if(responsePacket[2] == true)
    {
        std::cout << "Transmission started:" << std::endl;
    }
    else
    {
        //The request was denied, insert error handling
        std::cout << "Request denied. Exiting" << std::endl;
        return 1;
    }

    double x, y, z, w, rx, ry, rz;

    for (int i = 0; i < 5; i++)
    {
        recvPacket(responsePacket);
        x  = bytesToDouble(&responsePacket[2]);
        y  = bytesToDouble(&responsePacket[10]);
        z  = bytesToDouble(&responsePacket[18]);
        w  = bytesToDouble(&responsePacket[26]);
        rx = bytesToDouble(&responsePacket[34]);
        rz = bytesToDouble(&responsePacket[42]);
        ry = bytesToDouble(&responsePacket[50]);

        std::cout << std::fixed << std::setprecision(5) << "[" << x << ", " << y << ", " << z << ", " 
                  << w << ", " << rx << ", " << ry << ", " << rz << "]" << std::endl;
    }

    std::cout << "Requesting stop of transmission" << std::endl;
    requestPacket[0] = 0x03;
    requestPacket[1] = NRS_PT_CMD_TRANSMIT;
    requestPacket[2] = NRS_PT_CMD_TRANSMIT_STOP;

    send(socket_desc, requestPacket, requestPacket[0], 0);

    do{
        recvPacket(responsePacket);
    } while (responsePacket[1] != NRS_PT_CMD_TRANSMIT);

    close(socket_desc);

    return 0;
}

double bytesToDouble(const char *bytes)
{
    double data;

    unsigned char* ptr = (unsigned char*)(&data);
    for(int i = 7; i >= 0; --i) {
        *ptr = bytes[i] & 0xFF;
        ptr++;
    }

    return data;
}

void recvPacket(char *msg) {
    size_t received = recv(socket_desc, msg, 2, 0);

    while(received < msg[0]) {
        received += recv(socket_desc, &msg[received], msg[0] - received, 0);
    }
}
